Pod::Spec.new do |s|

s.name         = 'BaiduMapKit'
s.version      = '3.2.1'
s.summary      = '百度地图iOS SDK（CocoaPods百度地图官方库）'

s.description  = <<-DESC
百度地图iOS SDK：百度地图官方CocoaPods.\n百度地图iOS SDK是一套基于iOS 5.0及以上版本设备的应用程序接口，不仅提供展示地图的基本接口，还提供POI检索、路径规划、地图标注、离线地图、定位、周边雷达等丰富的LBS能力
DESC

s.homepage = 'http://developer.baidu.com/map/index.php?title=iossdk'

s.license  = { :type => 'MIT', :text => <<-LICENSE
Copyright © 2017 baidu. All Rights Reserved.
LICENSE
}

s.authors             = { 'baidu map sdk' => 'dituapi_01@163.com' }
s.platform            = :ios , '5.0'
s.source              = { :git => "https://bitbucket.org/tengpan/baidumapkit.git", :tag => s.version}
#s.source             = { :git => "https://bitbucket.org/tengpan/baidumapkit.git"}
s.source_files        = 'BaiduMapKit/*.framework/Headers/*.h'
s.public_header_files = 'BaiduMapKit/*.framework/Headers/*.h'
s.resources        = 'BaiduMapKit/BaiduMapAPI_Map.framework/Resources/mapapi.bundle'
s.vendored_frameworks = 'BaiduMapKit/*.framework'
s.vendored_libraries  = 'BaiduMapKit/thirdlibs/*.a'
s.frameworks          = 'CoreLocation', 'QuartzCore', 'OpenGLES', 'SystemConfiguration', 'CoreGraphics', 'Security', 'CoreTelephony'
s.libraries = 'sqlite3.0', 'stdc++.6.0.9'

s.requires_arc        = true

end
